import sys
import os
from cx_Freeze import setup, Executable

#files = ['icon.ico']

target = Executable(
    script = "HandLandmarkDetection.py",
    base = "Win32GUI",
)

setup(
    name = "HandLandmarkDetection",
    version = "0.1.0.0",
    description = "Camera Hand Detection for Unity",
    author = "Emanuel Antonoglou",
    #options = {'build_exe' : {'include_files' : files}},
    executables = [target]
)
