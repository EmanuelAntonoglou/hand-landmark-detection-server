using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationMenu : MonoBehaviour
{
    public Toggle toggleButton;

    void Start()
    {
        if (toggleButton != null)
        {
            toggleButton.onValueChanged.AddListener(OnToggleValueChanged);
        }
    }

    public void OnToggleValueChanged(bool value)
    {
        if (value)
        {
            WebSocketClient.i.SendMessageToServer("FlipCamera", false);
        }
        else
        {
            WebSocketClient.i.SendMessageToServer("FlipCamera", true);
        }
    }

    public void OnRestartButtonPressed(bool value)
    {
        LevelManager.i.RestartLevel();
    }
}
