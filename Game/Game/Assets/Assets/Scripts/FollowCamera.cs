using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [NonSerialized] private GameObject player;
    [NonSerialized] private Vector3 offset;
    [SerializeField] public bool followPlayerY = false;
    [SerializeField] public bool followPlayerX = false;

    private void Start()
    {
        player = GameManager.i.player.gameObject;
        offset = transform.position;
    }

    void Update()
    {
        if (LevelManager.i.finishedSpawning)
        {
            if (followPlayerY)
            {
                FollowPlayerY();
            }
            if (followPlayerX)
            {
                FollowPlayerX();
            }
        }
        else
        {
            followPlayerX = false;
            followPlayerY = false;
        }
    }

    private void FollowPlayerY()
    {
        transform.position = new Vector3(transform.position.x, player.transform.position.y + 2.4f, transform.position.z);
    }

    private void FollowPlayerX()
    {
        transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
    }
}
