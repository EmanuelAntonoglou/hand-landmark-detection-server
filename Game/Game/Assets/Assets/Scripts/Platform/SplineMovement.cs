using System;
using UnityEngine;
using UnityEngine.Splines;
using System.Collections;
using Unity.Mathematics;

[RequireComponent(typeof(SplineContainer))]
public class SplineMovement : MonoBehaviour
{
    #region Properties
    [Header("Spline Data")]
    [NonSerialized] private SplineContainer spline;
    [NonSerialized] public Spline knots;

    [Header("Movement Data")]
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float waitTime = 3.0f;
    [SerializeField] private bool LoopMovement = false;
    [NonSerialized] private GameObject objectToMove;
    [NonSerialized] private int currentKnotIndex = 0;
    [NonSerialized] private bool move = true;
    [NonSerialized] private bool returnToStart = false;
    #endregion

    private void Awake()
    {
        spline = GetComponent<SplineContainer>();
        knots = spline.Spline;
        objectToMove = transform.GetChild(0).GetChild(0).gameObject;
    }

    void Update()
    {
        MoveAlongSpline();
    }

    private void MoveAlongSpline()
    {
        if (currentKnotIndex == (knots.Count - 1))
        {
            returnToStart = true;
        }
        else if (currentKnotIndex == 0)
        {
            returnToStart = false;
        }

        float3 nextKnotPositionF;
        if (!returnToStart)
        {
            nextKnotPositionF = knots[currentKnotIndex + 1].Position;
        }
        else
        {
            nextKnotPositionF = knots[currentKnotIndex - 1].Position;
        }

        Vector3 nextKnotPosToV3 = new Vector3(nextKnotPositionF.x, nextKnotPositionF.y, nextKnotPositionF.z);
        Vector3 nextKnotPositionV3 = transform.position + nextKnotPosToV3;

        if (move)
        {
            if (objectToMove.transform.position == nextKnotPositionV3)
            {
                move = false;
                StartCoroutine(WaitForNextKnot());
            }
            else
            {
                float step = speed * Time.deltaTime;
                objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, nextKnotPositionV3, step);
            }
        }
    }

    IEnumerator WaitForNextKnot()
    {
        yield return new WaitForSeconds(waitTime);
        if (!returnToStart)
        {
            currentKnotIndex++;
        }
        else
        {
            currentKnotIndex--;
        }
        move = true;
    }

}


