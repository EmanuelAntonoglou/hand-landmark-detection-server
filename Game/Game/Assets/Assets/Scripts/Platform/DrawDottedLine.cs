using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Splines;

[RequireComponent(typeof(LineRenderer))]
public class DrawDottedLine : MonoBehaviour
{
    #region Properties
    [Header("Data Needed")]
    [NonSerialized] private LineRenderer lineRenderer;
    [NonSerialized] private Spline knots;
    #endregion

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        knots = GetComponent<SplineMovement>().knots;
        AddPositionsToLineRenderer();
    }

    void AddPositionsToLineRenderer()
    {
        lineRenderer.positionCount = knots.Count;

        for (int i = 0; i < knots.Count; i++)
        {
            float3 knotPositionF = knots[i].Position;
            Vector3 knotPositionV3 = new Vector3(knotPositionF.x, knotPositionF.y, knotPositionF.z);
            Vector3 nextKnotPositionV3 = transform.position + knotPositionV3;
            lineRenderer.SetPosition(i, nextKnotPositionV3);
        }
    }
}
