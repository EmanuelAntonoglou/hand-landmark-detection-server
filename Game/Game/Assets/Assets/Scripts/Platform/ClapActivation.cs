using System;
using UnityEngine;

public class ClapActivation : MonoBehaviour
{
    [SerializeField] private bool state = false;
    [NonSerialized] private bool changeStateFlag = false;
    [NonSerialized] private float updateTimer = 0f;

    [Header("COMPONENTS")]
    [NonSerialized] private MeshRenderer meshRenderer;
    [NonSerialized] private Material mat;
    [NonSerialized] private Collider col;
    [NonSerialized] private Outline outline;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        col = GetComponent<Collider>();
        outline = GetComponent<Outline>();
        mat = meshRenderer.material;
    }

    private void Update()
    {
        updateTimer += Time.deltaTime;

        if (updateTimer >= 0.025f)
        {
            if (LevelManager.i.clapActivated == state && !changeStateFlag)
            {
                changeStateFlag = true;
                col.enabled = true;

                Material[] newMaterials = new Material[meshRenderer.materials.Length + 1];
                Array.Copy(meshRenderer.materials, newMaterials, meshRenderer.materials.Length);
                newMaterials[newMaterials.Length - 1] = mat;
                meshRenderer.materials = newMaterials;
            }
            else if (LevelManager.i.clapActivated != state)
            {
                changeStateFlag = false;
                col.enabled = false;

                outline.enabled = false;
                meshRenderer.materials = new Material[0];
                outline.enabled = true;
            }
        }
    }
}