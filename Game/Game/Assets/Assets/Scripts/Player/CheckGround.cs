using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using static UnityEngine.UI.Image;

public class CheckGround : MonoBehaviour
{
    #region Properties
    [NonSerialized] private PlayerController player;

    [Header("SPHERE RAYCAST DATA")]
    [SerializeField] private float thickness = 0.5f;
    [SerializeField] private float maxDistance = 1.5f;
    [SerializeField] private LayerMask layersToHit;
    [NonSerialized] public int jumps = 1;
    [NonSerialized] public bool grounded = false;
    [NonSerialized] private bool onPlatform = false;

    [Header("MUSHROOM JUMP PARAMETERS")]
    [SerializeField] private float jumpForce = 15.0f;
    #endregion

    private void Awake()
    {
        player = GetComponent<PlayerController>();
    }

    void Update()
    {
        CheckForGround();
    }

    private void OnDrawGizmos()
    {
        Vector3 origin = transform.position;
        Vector3 direction = -transform.up;
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(origin + direction * maxDistance, thickness / 2);
    }

    private void CheckForGround()
    {
        RaycastHit hit;
        Vector3 origin = transform.position;
        Vector3 direction = -transform.up;

        if (Physics.SphereCast(origin, thickness / 2, direction, out hit, maxDistance, layersToHit))
        {
            if (!grounded)
            {
                LevelManager.i.finishedSpawning = true;
                grounded = true;
                jumps = 1;
                player.parabolicJump.ResetTrajectory();
                if (!LevelManager.i.wallSpawn.activeSelf)
                {
                    LevelManager.i.wallSpawn.SetActive(true);
                    GameManager.i.AttachAsChild(LevelManager.i.mainCamera.gameObject, GameManager.i.player.gameObject);

                }

                if (hit.collider.tag == "MovingPlatform" && !onPlatform)
                {
                    onPlatform = true;
                    GameObject platform = hit.collider.gameObject;
                    AttachAsChild(platform);
                }
                else if (hit.collider.tag == "Mushroom")
                {
                    player.rb.velocity = Vector3.zero;
                    player.rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                }
            }
        }
        else
        {
            grounded = false;
            if (onPlatform)
            {
                onPlatform = false;
                AttachAsChild(GameManager.i.level);
            }
        }
    }

    public void AttachAsChild(GameObject parent)
    {
        gameObject.transform.SetParent(parent.transform);
    }
}
