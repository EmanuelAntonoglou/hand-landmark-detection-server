using System;
using UnityEngine;

public class ParabolicJump : MonoBehaviour
{
    #region Properties
    [NonSerialized] private PlayerController player;

    [Header("TRAJECTORY JUMP")]
    [SerializeField] [Range(MINLAUNCHSPEED, MAXLAUNCHSPEED)] private float launchSpeed = 0.0f;
    [SerializeField] public float launchAngle = 0.0f;
    [NonSerialized] private const float MINLAUNCHSPEED = 5.0f;
    [NonSerialized] private const float MAXLAUNCHSPEED = 13.5f;
    [NonSerialized] public Transform launchPoint;

    [Header("TRAJECTORY DISPLAY")]
    [SerializeField] private int linePoints = 175;
    [SerializeField] private float timeIntervalInPoints = 0.01f;
    [SerializeField] private Color minLaunchColor;
    [SerializeField] private Color maxLaunchColor;
    [NonSerialized] public bool freezeParabole = true;
    [NonSerialized] public LineRenderer lineRenderer;
    #endregion

    private void Awake()
    {
        player = GetComponent<PlayerController>();
        lineRenderer = transform.Find("Parabole Trail").GetComponent<LineRenderer>();
        launchPoint = transform.Find("Launching Point").transform;
    }

    private void Start()
    {
        launchAngle = launchPoint.rotation.z;
    }

    void Update()
    {
        launchPoint.rotation = Quaternion.Euler(0, 0, launchAngle);
        if (lineRenderer != null && !freezeParabole)
        {
            DrawTrajectory();
            UpdateTrailColor();
        }
    }

    public void Jump()
    {
        player.rb.velocity = launchSpeed * launchPoint.up;
        freezeParabole = true;
        GameManager.i.player.checkGround.jumps--;
    }

    void DrawTrajectory()
    {
        Vector3 origin = launchPoint.position;
        Vector3 startVelocity = launchSpeed * launchPoint.up;
        lineRenderer.positionCount = linePoints;
        float time = 0;
        for (int i = 0; i < linePoints; i++)
        {
            // s = u*t + 1/2*g*t*t
            var x = (startVelocity.x * time) + (Physics.gravity.x / 2 * time * time);
            var y = (startVelocity.y * time) + (Physics.gravity.y / 2 * time * time);
            Vector3 point = new Vector3(x, y, 0);
            lineRenderer.SetPosition(i, origin + point);
            time += timeIntervalInPoints;
        }
    }

    public void ResetTrajectory()
    {
        Vector3 origin = launchPoint.position;
        for (int i = 0; i < Mathf.Min(linePoints, lineRenderer.positionCount); i++)
        {
            lineRenderer.SetPosition(i, origin);
        }
    }

    private void UpdateTrailColor()
    {
        // Clampea el valor de launchSpeed entre 0 y 1 para asegurarse de que est� dentro del rango v�lido.
        float normalizedLaunchSpeed = Mathf.Clamp01((launchSpeed - MINLAUNCHSPEED) / (MAXLAUNCHSPEED - MINLAUNCHSPEED));
        // Interpola entre los colores minLaunchColor y maxLaunchColor basado en el valor de launchSpeed.
        Color lerpedColor = Color.Lerp(minLaunchColor, maxLaunchColor, normalizedLaunchSpeed);

        Gradient tempGradient = lineRenderer.colorGradient;
        GradientColorKey[] tempColorKeys = tempGradient.colorKeys;
        for (int i = 0; i < tempColorKeys.Length; i++)
        {
            tempColorKeys[i].color = lerpedColor;
        }
        tempGradient.colorKeys = tempColorKeys;
        lineRenderer.colorGradient = tempGradient;
    }

    public void ChangeSpeed(float pinchDistance)
    {
        float normalizedDistance = pinchDistance * 2f;
        launchSpeed = MINLAUNCHSPEED + (normalizedDistance * (MAXLAUNCHSPEED - MINLAUNCHSPEED));
        launchSpeed = Mathf.Clamp(launchSpeed, MINLAUNCHSPEED, MAXLAUNCHSPEED);
    }

    public void ChangeAngle(float distanciaX)
    {
        float MINANGLE = -90f;
        float MAXANGLE = 90f;

        float normalizedDistance = (distanciaX + 0.5f) / 1.0f;
        launchAngle = MINANGLE + normalizedDistance * (MAXANGLE - MINANGLE);
        launchAngle = Mathf.Clamp(launchAngle, MINANGLE, MAXANGLE);
    }
}