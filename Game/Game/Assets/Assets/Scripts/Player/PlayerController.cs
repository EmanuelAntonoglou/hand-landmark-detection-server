using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("COMPONENTS")]
    [NonSerialized] public ParabolicJump parabolicJump;
    [NonSerialized] public Rigidbody rb;
    [NonSerialized] public CheckGround checkGround;
    [NonSerialized] public FollowCamera followCamera;

    private void Awake()
    {
        parabolicJump = GetComponent<ParabolicJump>();
        rb = GetComponent<Rigidbody>();
        checkGround = GetComponent<CheckGround>();
        followCamera = LevelManager.i.mainCamera.GetComponent<FollowCamera>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            LevelManager.i.RespawnCheckpoint();
        }
        else if (collision.gameObject.tag == "Fork")
        {
            LevelManager.i.RespawnCheckpoint();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Star")
        {
            other.gameObject.SetActive(false);
            LevelManager.i.currentStars++;
            LevelManager.i.ChangeTMPStars();
        }
        if (other.tag == "Win")
        {
            LevelManager.i.WinLevel();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "FollowCamera")
        {
            GameManager.i.AttachAsChild(LevelManager.i.mainCamera.gameObject, GameManager.i.player.gameObject);
        }
        if (other.tag == "StopCameraX")
        {
            LevelManager.i.mainCamera.gameObject.transform.parent = null;
            followCamera.followPlayerX = false;
            followCamera.followPlayerY = true;
        }
        if (other.tag == "StopCamera")
        {
            LevelManager.i.mainCamera.gameObject.transform.parent = null;
            followCamera.followPlayerX = false;
            followCamera.followPlayerY = false;
        }

    }
}
