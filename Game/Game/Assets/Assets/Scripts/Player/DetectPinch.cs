using UnityEngine;
using System;
using Unity.VisualScripting;
using System.Linq;

public class DetectPinch : MonoBehaviour
{
    #region Properties
    [NonSerialized] private PlayerController player;

    [Header("PINCH PARAMETERS")]
    [SerializeField] private float pinchCount = 30.0f;
    [NonSerialized] private bool pinched = false;
    [NonSerialized] private Vector3 pinchPos = Vector3.zero;
    [NonSerialized] private Hand pinchHand = null;
    #endregion


    private void Awake()
    {
        player = GetComponent<PlayerController>();
    }

    float timerDuration = 0.3f;
    float time = 0.0f;
    bool startTimer = false;

    void Update()
    {
        if (LevelManager.i.finishedSpawning)
        {
            DetectHand();
            Vector3 realtimePos = GetRealtimePos();
            float adjustedPinchThreshold = pinchCount * ((1 - realtimePos.z) * 2.5f);

            if (pinchHand != null && !pinchHand.fist && time <= 0)
            {
                time = 0;
                startTimer = false;

                if (player.checkGround.jumps > 0)
                {
                    player.parabolicJump.freezeParabole = false;
                }

                if (pinchHand.distance < adjustedPinchThreshold && player.checkGround.jumps > 0)
                {
                    if (!pinched)
                    {
                        StartPinching();
                    }
                    RealtimeSpeedAngle(realtimePos);
                }
                else if (pinchHand != null && pinchHand.distance > adjustedPinchThreshold)
                {
                    if (pinched && player.checkGround.jumps > 0)
                    {
                        pinched = false;
                        player.parabolicJump.Jump();
                    }
                }
                UIManager.i.ChangeTMPText(UIManager.i.TMPThresholdR, Math.Round(adjustedPinchThreshold, 2).ToString());
            }
            else if (pinchHand != null && pinchHand.fist)
            {
                pinched = false;
                startTimer = true;
                time = timerDuration;
            }

            if (startTimer)
            {
                time -= Time.deltaTime;
                if (player.checkGround.jumps > 0)
                {
                    player.parabolicJump.ResetTrajectory();
                }
                player.parabolicJump.freezeParabole = true;
            }
        }
    }

    private void DetectHand()
    {
        if (!GameManager.i.handL.visible && !GameManager.i.handR.visible)
        {
            pinchHand = null;
        }
        else if (pinchHand == null && GameManager.i.handL.visible)
        {
            pinchHand = GameManager.i.handL;
        }
        else if (pinchHand == null && GameManager.i.handR.visible)
        {
            pinchHand = GameManager.i.handR;
        }
    }

    private void RealtimeSpeedAngle(Vector3 realtimePos)
    {
        float distanciaX = (realtimePos.x - pinchPos.x) * -1;
        float distanciaY = MathF.Abs(realtimePos.y - pinchPos.y);

        player.parabolicJump.ChangeSpeed(distanciaY);
        player.parabolicJump.ChangeAngle(distanciaX);
    }

    private Vector3 GetRealtimePos()
    {
        if (pinchHand != null)
        {
            Vector3 pos4 = pinchHand.points[4].position;
            Vector3 pos8 = pinchHand.points[8].position;
            return GetAverage(pos4, pos8);
        }
        return Vector3.zero;
    }

    private void StartPinching()
    {
        pinched = true;
        player.parabolicJump.lineRenderer.enabled = true;
        player.parabolicJump.freezeParabole = false;
        Vector3 pos4Pinch = pinchHand.points[4].position;
        Vector3 pos8Pinch = pinchHand.points[8].position;
        pinchPos = GetAverage(pos4Pinch, pos8Pinch);
    }

    private Vector3 GetAverage(Vector3 v1, Vector3 v2)
    {
        Vector3 average = new Vector3(
            (v1.x + v2.x) / 2f,
            (v1.y + v2.y) / 2f,
            (v1.z + v2.z) / 2f
        );
        return average;
    }
}
