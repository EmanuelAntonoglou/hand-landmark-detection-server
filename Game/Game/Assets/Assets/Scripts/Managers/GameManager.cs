using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [NonSerialized] public static GameManager i;

    [Header("LEVEL DATA")]
    [SerializeField] public GameObject level;
    [SerializeField] public PlayerController player;

    [Header("HAND DATA")]
    [NonSerialized] public Hand handL = new Hand();
    [NonSerialized] public Hand handR = new Hand();
    [NonSerialized] public bool clap = false;

    private Process HLDProcess;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        OpenExternalApp();
    }

    private void OpenExternalApp()
    {
        string pathToApp = Path.Combine(Application.streamingAssetsPath, "HLD Process", "HandLandmarkDetection.exe");

        // Como la ruta de streamingAssetsPath usa "file:///", necesitamos eliminarlo para que funcione correctamente con Process.Start
        pathToApp = pathToApp.Replace("file:///", "");

        if (System.IO.File.Exists(pathToApp))
        {
            HLDProcess = Process.Start(pathToApp);
        }
        else
        {
            UnityEngine.Debug.LogError("El archivo de la aplicación externa no existe en la ubicación especificada: " + pathToApp);
        }
    }

    private void OnApplicationQuit()
    {
        if (HLDProcess != null && !HLDProcess.HasExited)
        {
            HLDProcess.Kill();
            HLDProcess.Dispose();
        }
    }

    public void AttachAsChild(GameObject child, GameObject parent)
    {
        child.transform.SetParent(parent.transform);
    }

    private void Update()
    {
        if (clap)
        {
            UnityEngine.Debug.Log("Clap");
            clap = false;
            LevelManager.i.clapActivated = !LevelManager.i.clapActivated;
        }
    }
}
