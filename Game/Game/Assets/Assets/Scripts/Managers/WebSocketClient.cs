using System.Collections.Generic;
using UnityEngine;
using WebSocketSharp;
using Newtonsoft.Json.Linq;
using Unity.VisualScripting;
using System.Globalization;
using System;
using Newtonsoft.Json;

public class WebSocketClient : MonoBehaviour
{
    #region Properties
    [NonSerialized] public static WebSocketClient i;

    [Header("CLIENT DATA")]
    [SerializeField] private string serverURL = "ws://localhost:5000";
    [NonSerialized] private WebSocket ws;
    [NonSerialized] private bool isConnected = false;

    [Header("TEMPORARY DATA")]
    [NonSerialized] private int clapCount = 0;

    #endregion

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        // Crear una nueva instancia de WebSocket y conectar al servidor
        ws = new WebSocket(serverURL);

        // Definir los m�todos de manejo de eventos
        ws.OnOpen += OnWebSocketOpen;
        ws.OnMessage += OnMessageReceived;
        ws.OnError += OnError;
        ws.OnClose += OnWebSocketClose;

        InvokeRepeating("ConnectToServer", 0.0f, 3.0f);
    }

    void ConnectToServer()
    {
        if (!isConnected)
        {
            ws.Connect();
        }
    }

    // Este m�todo se llama cuando la conexi�n WebSocket se abre con �xito
    private void OnWebSocketOpen(object sender, System.EventArgs e)
    {
        isConnected = true;
        Debug.Log("Conexi�n WebSocket abierta.");
        CancelInvoke("ConnectToServer");
    }

    // Este m�todo se llama cuando se recibe un mensaje del servidor WebSocket
    private void OnMessageReceived(object sender, MessageEventArgs e)
    {
        ParseJSON(e.Data);
    }

    // Este m�todo se llama cuando se produce un error en la conexi�n WebSocket
    private void OnError(object sender, ErrorEventArgs e)
    {
        Debug.LogWarning("Error en la conexi�n WebSocket: " + e.Message);
    }

    // Este m�todo se llama cuando se cierra la conexi�n WebSocket
    private void OnWebSocketClose(object sender, CloseEventArgs e)
    {
        if (isConnected)
        {
            Debug.Log("Conexi�n WebSocket cerrada. C�digo: " + e.Code + ", Raz�n: " + e.Reason);
        }
        else
        {
            Debug.Log("No se pudo conectar al WebSocket. Intentando de nuevo en 3 segundos...");
        }
        isConnected = false;
    }

    // Cerrar la conexi�n WebSocket cuando el objeto es destruido
    private void OnDestroy()
    {
        if (ws != null && ws.IsAlive)
        {
            ws.Close();
        }
    }

    public void SendMessageToServer(string type, object data)
    {
        if (ws != null && ws.IsAlive)
        {
            var messageObject = new
            {
                type = type,
                data = data
            };

            string jsonMessage = JsonConvert.SerializeObject(messageObject);
            ws.Send(jsonMessage);
        }
        else
        {
            Debug.LogError("No se puede enviar el mensaje: WebSocket no est� conectado.");
        }
    }

    void ParseJSON(string json)
    {
        JObject jsonData = JObject.Parse(json);
        string type = jsonData["type"].ToString();
        JObject data = jsonData["data"] as JObject;

        if (type == "HandPoints")
        {
            Point new_point = new Point();
            new_point.index = data["index"].Value<int>();
            new_point.position.x = data["x"].Value<float>();
            new_point.position.y = data["y"].Value<float>();
            new_point.position.z = data["z"].Value<float>();

            if (data["hand"].ToString() == "Left")
            {
                GameManager.i.handL.handType = "Left";
                GameManager.i.handL.points[new_point.index] = new_point;
            }
            else if (data["hand"].ToString() == "Right")
            {
                GameManager.i.handR.handType = "Right";
                GameManager.i.handR.points[new_point.index] = new_point;
            }
        }
        if (type == "Hand")
        {
            GameManager.i.handL.visible = data["visibleL"].Value<bool>();
            GameManager.i.handR.visible = data["visibleR"].Value<bool>();
            GameManager.i.handL.distance = data["distanceL"].Value<float>();
            GameManager.i.handR.distance = data["distanceR"].Value<float>();
            GameManager.i.handL.fist = data["fistL"].Value<bool>();
            GameManager.i.handR.fist = data["fistR"].Value<bool>();
        }
        if (type == "Clap")
        {
            if (data["clapCount"].Value<int>() > clapCount)
            {
                GameManager.i.clap = true;
                clapCount = data["clapCount"].Value<int>();
            }
        }
    }
}