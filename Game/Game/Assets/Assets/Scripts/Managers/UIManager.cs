using UnityEngine;
using TMPro;
using System;

public class UIManager : MonoBehaviour
{
    #region Properties
    [NonSerialized] public static UIManager i;

    [Header("LEVEL")]
    [SerializeField] public TextMeshProUGUI TMPStars;
    [SerializeField] public TextMeshProUGUI TMPWin;

    [Header("DEBUG")]
    [SerializeField] public TextMeshProUGUI TMPThresholdR;
    [SerializeField] public TextMeshProUGUI TMPDisL;
    [SerializeField] public TextMeshProUGUI TMPDisR;
    [SerializeField] public TextMeshProUGUI TMPFistL;
    [SerializeField] public TextMeshProUGUI TMPFistR;
    #endregion


    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        TMPDisL.text = GameManager.i.handL.distance.ToString();
        TMPDisR.text = GameManager.i.handR.distance.ToString();
        TMPFistL.gameObject.SetActive(GameManager.i.handL.fist);
        TMPFistR.gameObject.SetActive(GameManager.i.handR.fist);
    }

    public void ChangeTMPText(TextMeshProUGUI TMP, string text)
    {
        TMP.text = text;
    }
}
