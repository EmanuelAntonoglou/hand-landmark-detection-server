using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [NonSerialized] public static LevelManager i;
    [NonSerialized] private PlayerController player;

    [Header("LEVEL DATA")]
    [SerializeField] public GameObject level;
    [SerializeField] public GameObject wallSpawn;
    [NonSerialized] public bool clapActivated = false;
    [NonSerialized] public List<GameObject> stars = new List<GameObject>();
    [NonSerialized] public int totalStars = 0;
    [NonSerialized] public int currentStars = 0;

    [Header("RESPAWN")]
    [SerializeField] public Camera mainCamera;
    [NonSerialized] public bool finishedSpawning = false;
    [NonSerialized] public Vector3 checkpointPos;
    [NonSerialized] private Vector3 mainCameraPos;
    [NonSerialized] private Vector3 startPos;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        player = GameManager.i.player;
        startPos = level.transform.Find("StartPoint").transform.position;
        mainCameraPos = mainCamera.transform.position;
        checkpointPos = startPos;
        CountStarts();
        Jump();
    }

    public void Jump()
    {
        player.parabolicJump.launchPoint.rotation = Quaternion.Euler(0, 0, 45);
        player.parabolicJump.freezeParabole = true;
        player.rb.velocity = 10f * player.parabolicJump.launchPoint.up;
    }

    private void CountStarts()
    {
        GameObject starParent = level.transform.Find("Stars").gameObject;
        foreach (Transform starTransform in starParent.transform)
        {
            stars.Add(starTransform.gameObject);
        }
        totalStars = stars.Count;
        ChangeTMPStars();
    }

    public void ChangeTMPStars()
    {
        UIManager.i.ChangeTMPText(UIManager.i.TMPStars, currentStars.ToString() + "/" + totalStars.ToString() + " Stars");
    }

    public void RespawnCheckpoint()
    {
        UIManager.i.TMPWin.gameObject.SetActive(false);
        finishedSpawning = false;
        wallSpawn.SetActive(false);
        ResetPlayer(checkpointPos);
        Jump();
    }

    public void RestartLevel()
    {
        UIManager.i.TMPWin.gameObject.SetActive(false);
        finishedSpawning = false;

        foreach (GameObject star in stars)
        {
            star.SetActive(true);
        }

        wallSpawn.SetActive(false);
        currentStars = 0;
        ChangeTMPStars();
        ResetPlayer(startPos);
        Jump();
    }

    private void ResetPlayer(Vector3 pos)
    {
        player.rb.velocity = Vector3.zero;
        player.parabolicJump.ResetTrajectory();
        player.transform.position = pos;

        mainCamera.transform.position = mainCameraPos;
    }

    public void WinLevel()
    {
        UIManager.i.TMPWin.gameObject.SetActive(true);
        mainCamera.GetComponent<FollowCamera>().followPlayerY = false;
        player.parabolicJump.ResetTrajectory();
    }
}
