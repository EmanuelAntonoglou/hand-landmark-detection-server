[System.Serializable]
public class Hand
{
    public string handType = "Left";
    public bool visible = false;
    public Point[] points = new Point[21];
    public float distance = 0.0f;
    public bool fist = false;

    public Hand()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = new Point();
        }
    }

}
