using UnityEngine;

[System.Serializable]
public class Point
{
    public int index = 0;
    public Vector3 position = Vector3.zero;
}
