import speech_recognition as sr
import pyttsx3

r = sr.Recognizer()

def record_text():
    try:
        # Listen for a single phrase with a very short timeout
        with sr.Microphone() as source:
            audio = r.listen(source, phrase_time_limit=1.75)  # Adjust as needed

        # Recognize the recorded audio
        MyText = r.recognize_google(audio, language="es-ES")

        # Lowercase the recognized text for case-insensitive comparison
        MyText = MyText.lower()
        print(MyText)

        # Split text and take the first word if necessary
        if len(MyText.split()) > 1:
            single_word = MyText.split()[0]
            return single_word  # Return only the first word
        else:
            return MyText  # Return the single-word utterance

    except sr.RequestError as e:
        print("Could not request results; {0}".format(e))
        return None  # Indicate failure
    except sr.UnknownValueError:
        print("Unknown Error Occurred")
        return None  # Indicate failure

while True:
    single_word = record_text()
    if single_word:  # Check if single_word is not None (indicates failure)
        # Process or print the single word
        print(f"Single word: {single_word}")
