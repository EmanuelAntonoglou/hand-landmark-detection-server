import mediapipe as mp
import cv2
import numpy as np
import math

mpPose = mp.solutions.pose
mpDrawing = mp.solutions.drawing_utils
cap = cv2.VideoCapture(0)
pose = mpPose.Pose(min_detection_confidence = 0.8, min_tracking_confidence = 0.8)  # Ajusta los umbrales de confianza

##Clap counter variables
clapCounter = 0
clapThreshold = 0.28
farThreshold = 0.30
clapStage = None

def distance(a,b):
    dist = math.hypot(a[1]-a[0], b[1]-b[0])
    return round(dist,2)

def DrawClap(dist):
    cv2.rectangle(image, (0,0), (255,90), (0,0,250), -1)

    # Clap Counter
    cv2.putText(image, str(clapCounter), (10,65), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 2, cv2.LINE_AA)
        
    # Stage Data
    cv2.putText(image, clapStage, (100,65), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 2, cv2.LINE_AA)
    
    # Visualize Angle
    cv2.putText(image, str(dist), tuple(np.multiply(shoulder, [640, 480]).astype(int)), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)

    # Render Detections
    mpDrawing.draw_landmarks(image, results.pose_landmarks, mpPose.POSE_CONNECTIONS,
                                mpDrawing.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=2), 
                                mpDrawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2))               
        
    cv2.imshow('Clap Detection', image)

## Setup Mediapipe Instance
while cap.isOpened():
    ret, frame = cap.read()
    
    # Recolor image to RGB
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    image.flags.writeable = False
  
    # Make detection
    results = pose.process(image)

    # Recolor back to BGR
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    
    #Extract landmarks
    try:
        landmarks = results.pose_landmarks.landmark
        
        # Get coordinates
        left_index=[landmarks[mpPose.PoseLandmark.LEFT_INDEX.value].x,landmarks[mpPose.PoseLandmark.LEFT_INDEX.value].y]
        right_index=[landmarks[mpPose.PoseLandmark.RIGHT_INDEX.value].x,landmarks[mpPose.PoseLandmark.RIGHT_INDEX.value].y]
        shoulder = [landmarks[mpPose.PoseLandmark.LEFT_SHOULDER.value].x,landmarks[mpPose.PoseLandmark.LEFT_SHOULDER.value].y]
        
        # Calculate distance
        dist = distance(left_index,right_index)
                    
        # Counter Logic
        if dist > farThreshold:
            clapStage = "far"
        elif dist < clapThreshold and clapStage == 'far':
            clapStage = "clap"
            clapCounter += 1
        
        DrawClap(dist)
        
    except:
        pass
    
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
