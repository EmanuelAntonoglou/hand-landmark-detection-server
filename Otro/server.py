import asyncio
import websockets
import datetime

async def handler(websocket, path):
    await websocket.send("Server Conectado")

    while True:
        now = datetime.datetime.now().strftime("%H:%M:%S")
        await websocket.send(now)
        await asyncio.sleep(3)  # Wait for 3 seconds

async def main():
    async with websockets.serve(handler, "localhost", 5000):
        print("Server Iniciado")
        
        await asyncio.Future()  # run forever

asyncio.run(main())
