import socket

host = 'localhost'
port = 1234
HEADERSIZE = 10

def connect_to_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))

        full_msg = ""
        new_msg = True
        while True:
            msg = s.recv(16)
            if new_msg:
                #print(f"New message lenght: {msg[:HEADERSIZE]}") #Print cantidad de bytes
                msglen = int(msg[:HEADERSIZE])
                new_msg = False
            full_msg += msg.decode("utf-8")

            if len(full_msg)-HEADERSIZE == msglen:
                #print("Full message recived") #Print mensaje recibido
                print(full_msg[HEADERSIZE:])
                new_msg = True
                full_msg = ""
            


if __name__ == '__main__':
    connect_to_server()
