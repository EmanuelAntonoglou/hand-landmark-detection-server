import socket
import time

host = 'localhost'
port = 1234
HEADERSIZE = 10

def start_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen(5)

        while True:
            clientsocket, address = s.accept()
            print(f"Connection from {address} has been established!")

            msg = "Welcome to the server!"
            msg = f"{len(msg):<{HEADERSIZE}}" + msg
            clientsocket.send(bytes(msg, "utf-8"))

            while True:
                time.sleep(3)
                msg = f"The time is: {round(time.time())}"
                msg = f"{len(msg):<{HEADERSIZE}}" + msg
                clientsocket.send(bytes(msg, "utf-8"))


if __name__ == '__main__':
    start_server()
