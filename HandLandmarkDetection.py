import cv2
import mediapipe as mp
import threading
import math
import numpy as np

import asyncio
import websockets
import json

import keyboard
import time


cap = cv2.VideoCapture(0)
img = None
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode = False,
                      max_num_hands = 2,
                      min_detection_confidence = 0.9,
                      min_tracking_confidence = 0.8)
mpDraw = mp.solutions.drawing_utils
flipCameraFlag: bool = True

# Clap
mpPose = mp.solutions.pose
pose = mpPose.Pose(min_detection_confidence = 0.9, min_tracking_confidence = 0.9)
clapCounter = 0
clapThreshold = 0.23
farThreshold = 0.25

# Hand Data
HandPointsL = []
HandPointsR = []
numDecimals: int = 3
touch_threshold: float = 25.0
distanceL: float = 0.0
distanceR: float = 0.0
visibleL: bool = False
visibleR: bool = False
fistL: bool = False
fistR: bool = False

# Draw
initialTime = cv2.getTickCount()

# Server
serverInterval: float = 0.0


#region HandLandmarkDetection
def OpenApp():
    global img, flipCamera

    for i in range(21):
        data = {"hand": "Left", "index": 0, "x": 0.0, "y": 0.0, "z": 0.0}
        HandPointsL.append(data)
        HandPointsR.append(data)


    while cap.isOpened():
        success, img = cap.read()
        if flipCameraFlag:
            FlipCamera()
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        result = hands.process(imgRGB)

        GetLandmarkPositions(result, img)
        Clap(img, result)
        Draw(result, img)
        #DrawFPS(img)

        cv2.imshow("Hand Detection", img)
        key = cv2.waitKey(1) & 0xFF
        if key == 27:  # Exit if 'Esc' key is pressed (key code 27)
            break
    print("Programa Cerrado")

def GetLandmarkPositions(result, img):
    if result.multi_hand_landmarks:
        global fistL, fistR
        fistL = False
        fistR = False

        for handLms in result.multi_hand_landmarks:
            frame_height, frame_width, _ = img.shape  
            hand_points = {point_index: {"x": point.x, "y": point.y} for point_index, point in enumerate(handLms.landmark)}
            wrist_x = hand_points[0]["x"] * frame_width
            
            handType = "Left" if wrist_x < frame_width / 2 else "Right"
            
            lmList = []

            for i, landmark in enumerate(handLms.landmark):
                #Landmark Detection
                x = round(landmark.x, numDecimals)
                y = round(landmark.y, numDecimals)
                z = round(landmark.z, numDecimals)
 
                #Fist Detection
                h, w, c = img.shape
                cx, cy = int(landmark.x * w), int(landmark.y * h)
                lmList.append([i, cx, cy])

                #Hand Data
                data = {"hand": handType, "index": i, "x": x, "y": y, "z": z}
                if handType == "Left":
                    HandPointsL[i] = data
                elif handType == "Right":
                    HandPointsR[i] = data
            
            DetectClosedFist(lmList, handType, img)
            
def DetectClosedFist(lmList, handType, img):
    global fistL, fistR

    indexX = 0
    indexY = 0
    indexMid = 0
    handBottomX = 0
    handBottomY = 0
    pinkyX = 0
    pinkyY = 0 

    for lms in lmList:
        if lms[0] == 7:
            indexX, indexY = lms[1], lms[2]
        elif lms[0] == 5:
            indexMid = lms[2]
        elif lms[0] == 19:
            pinkyX, pinkyY = lms[1], lms[2]
        elif lms[0] == 0:
            handBottomX, handBottomY = lms[1], lms[2]

    if (indexY < handBottomY) and (indexY > indexMid):
        cv2.rectangle(img, (indexX, indexY), (pinkyX, handBottomY), (255, 0, 0), 2)
        cv2.putText(img, ("Fist: " + handType), (pinkyX + 2, indexY - 2), (cv2.FONT_HERSHEY_SIMPLEX), .7,
                    (255, 0, 0), 1, cv2.LINE_4)
        if (handType == "Left"):
            fistL = True
        if (handType == "Right"):
            fistR = True

def Draw(result, img):
    global visibleL, visibleR
    visibleL = False
    visibleR = False
    if result.multi_hand_landmarks:
        DrawPoints(result, img)

        for hand_landmarks in result.multi_hand_landmarks:
            DrawLine(img, hand_landmarks)

def DrawPoints(result, img):
    for handsLms in result.multi_hand_landmarks:
        mpDraw.draw_landmarks(img, handsLms, mpHands.HAND_CONNECTIONS)

def DrawLine(img, hand_landmarks):
    global distanceL, distanceR, visibleL, visibleR

    frame_height, frame_width, _ = img.shape  
    hand_points = {point_index: {"x": point.x, "y": point.y} for point_index, point in enumerate(hand_landmarks.landmark)}
    wrist_x = hand_points[0]["x"] * frame_width
    
    pointR4 = (int(HandPointsR[4]["x"] * frame_width), int(HandPointsR[4]["y"] * frame_height))
    pointR8 = (int(HandPointsR[8]["x"] * frame_width), int(HandPointsR[8]["y"] * frame_height))
    distanceR = math.sqrt((pointR4[0] - pointR8[0])**2 + (pointR4[1] - pointR8[1])**2)
    
    pointL4 = (int(HandPointsL[4]["x"] * frame_width), int(HandPointsL[4]["y"] * frame_height))
    pointL8 = (int(HandPointsL[8]["x"] * frame_width), int(HandPointsL[8]["y"] * frame_height))
    distanceL = math.sqrt((pointL4[0] - pointL8[0])**2 + (pointL4[1] - pointL8[1])**2)

    if wrist_x < frame_width / 2: # Left Hand
        if distanceL <= touch_threshold:
            color = (0,255,0)
        else:
            color = (0,0,255)
        cv2.putText(img, ("Distancia: " + f"{distanceL:.2f}"), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color, 2, cv2.LINE_AA)
        cv2.line(img, pointL4, pointL8, color, 5)
        visibleL = True
    else: # Right Hand
        if distanceR <= touch_threshold:
            color = (0,255,0)
        else:
            color = (0,0,255)
        cv2.putText(img, ("Distancia: " + f"{distanceR:.2f}"), (410, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color, 2, cv2.LINE_AA)
        cv2.line(img, pointR4, pointR8, color, 5)
        visibleR = True

def FlipCamera():
    global img
    img = cv2.flip(img, 1) 

#endregion

#region Clap
def distance(a,b):
    dist = math.hypot(a[1]-a[0], b[1]-b[0])
    return round(dist,2)

def DrawClap(image, results):
    global clapStage, clapCounter, shoulder, dist
    
    # Dimensiones de la imagen
    height, width, _ = image.shape
    
    # Dibujar un rectángulo en la esquina inferior izquierda
    cv2.rectangle(image, (0, height - 80), (170, height), (0, 0, 250), -1)

    # Contador de aplausos
    cv2.putText(image, str(clapCounter), (10, height - 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        
    # Estado del aplauso
    cv2.putText(image, clapStage, (80, height - 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    
    # Visualizar el ángulo
    cv2.putText(image, str(dist), tuple(np.multiply(shoulder, [width, height]).astype(int)), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)

    # Renderizar las detecciones
    mpDraw.draw_landmarks(image, results.pose_landmarks, mpPose.POSE_CONNECTIONS,
                                mpDraw.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=2), 
                                mpDraw.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2))               

## Setup Mediapipe Instance
def Clap(img, result):
    global clapStage, clapCounter, shoulder, dist
    results = pose.process(img)
    isPersonOnCamera = False

    try:
        landmarks = results.pose_landmarks.landmark
        
        # Get coordinates
        left_index=[landmarks[mpPose.PoseLandmark.LEFT_INDEX.value].x,landmarks[mpPose.PoseLandmark.LEFT_INDEX.value].y]
        right_index=[landmarks[mpPose.PoseLandmark.RIGHT_INDEX.value].x,landmarks[mpPose.PoseLandmark.RIGHT_INDEX.value].y]
        shoulder = [landmarks[mpPose.PoseLandmark.LEFT_SHOULDER.value].x,landmarks[mpPose.PoseLandmark.LEFT_SHOULDER.value].y]
        # Calculate distance
        dist = distance(left_index,right_index)
                    
        # Counter Logic
        if dist > farThreshold:
            clapStage = "far"
        elif dist < clapThreshold and clapStage == 'far':
            clapStage = "clap"
            clapCounter += 1
            print("Clap")
        
        if dist >= 0.30 and visibleL and visibleR:
            print("No Clap")
        
        isPersonOnCamera = True

        DrawClap(img, result)
        
    except:
        #print(isPersonOnCamera)
        isPersonOnCamera = False
        pass


def DrawFPS(frame):
  global initialTime
  
   # Calcular el tiempo actual
  currentTime = cv2.getTickCount()

  # Calcular el tiempo transcurrido
  elapsedTime = (currentTime - initialTime) / cv2.getTickFrequency()

  # Calcular los FPS
  fps = 1 / elapsedTime

  # Convertir FPS a string
  fps_str = f"FPS: {int(fps)}"

  # Dibujar los FPS en la pantalla
  cv2.putText(frame, fps_str, (520, 470), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)

  # Actualizar el tiempo inicial
  initialTime = currentTime

  return frame
#endregion

#region Server
async def StartServer(salir):
    async with websockets.serve(Handler, "localhost", 5000):
        print("Server Iniciado")
        while True:
            if salir.is_set():
                print("Servidor Cerrado")
                break
            await asyncio.sleep(0.1)

async def Handler(websocket, path):
    await websocket.send("Server Conectado")

    while True:
        await GetData(websocket)
        for i in range(21):
            await websocket.send(json.dumps({"type": "HandPoints", "data": HandPointsL[i]}))
        for i in range(21):
            await websocket.send(json.dumps({"type": "HandPoints", "data": HandPointsR[i]}))

        dataHand = { "visibleL": visibleL, "distanceL": f"{distanceL:.2f}", "visibleR": visibleR, "distanceR": f"{distanceR:.2f}", "fistL": fistL, "fistR": fistR}
        await websocket.send(json.dumps({"type": "Hand", "data": dataHand}))

        dataClap = { "clapCount": clapCounter }
        await websocket.send(json.dumps({"type": "Clap", "data": dataClap}))

        await asyncio.sleep(serverInterval)

async def GetData(websocket):
    global flipCameraFlag
    try:
        message = await asyncio.wait_for(websocket.recv(), timeout=0.1)
    except asyncio.TimeoutError:
        return
    
    message_data = json.loads(message)

    if message_data.get("type") == "FlipCamera":
        flipCamera = bool(message_data.get("data"))
        if flipCamera != flipCameraFlag:
            flipCameraFlag = flipCamera

#endregion

if __name__ == '__main__':
    # Iniciar hilos
    salir = threading.Event()
    thread_open_app = threading.Thread(target=OpenApp)
    thread_server = threading.Thread(target=asyncio.run, args=(StartServer(salir),))

    thread_open_app.start()
    thread_server.start()

    try:
        while True:
            if keyboard.is_pressed('esc'):
                salir.set() # Establecer la bandera para indicar que el servidor debe terminar
                break
            time.sleep(0.1)
    except KeyboardInterrupt:
        pass

    # Esperar a que los hilos terminen (opcional)
    thread_open_app.join()
    thread_server.join()