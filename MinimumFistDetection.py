import cv2
import mediapipe as mp


def cameravision():
    # 0 is your built-in webcam
    # 1 is an external webcam
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FPS, 30)

    # New for finger detection
    mpHands = mp.solutions.hands
    hands = mpHands.Hands()
    mpDraw = mp.solutions.drawing_utils

    try:
        while 1:
            font = cv2.FONT_HERSHEY_SIMPLEX
            success, img = cap.read()
            imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            results = hands.process(imgRGB)

            if results.multi_hand_landmarks:
                for handLMS in results.multi_hand_landmarks:
                    lmList = []
                    for id, lm in enumerate(handLMS.landmark):
                        h, w, c = img.shape
                        cx, cy = int(lm.x * w), int(lm.y * h)
                        lmList.append([id, cx, cy])
                    indexX = 0
                    indexY = 0
                    indexMid = 0
                    handBottomX = 0
                    handBottomY = 0
                    pinkyX = 0
                    pinkyY = 0
                    fistWarning = "Fist!"
                    for lms in lmList:
                        if lms[0] == 7:
                            indexX, indexY = lms[1], lms[2]
                        elif lms[0] == 5:
                            indexMid = lms[2]
                        elif lms[0] == 19:
                            pinkyX, pinkyY = lms[1], lms[2]
                        elif lms[0] == 0:
                            handBottomX, handBottomY = lms[1], lms[2]
                    if (indexY < handBottomY) and (indexY > indexMid):
                        cv2.rectangle(img, (indexX, indexY), (pinkyX, handBottomY), (0, 0, 255), 2)
                        cv2.putText(img, fistWarning, (pinkyX + 2, indexY - 2), (font), .7,
                                    (0, 0, 255), 1, cv2.LINE_4)
                        print("Fist!!")

            cv2.imshow("Your Project", img)

            k = cv2.waitKey(10) & 0xFF
            #  27 is the escape key
            if k == 27:
                break
            else:
                pass

        cap.release()

        cv2.destroyAllWindows()

    except cv2.error as e:
        print(str(e))


cameravision()
