extends Node

export var websocket_url = "ws://localhost:5000"

onready var parent = get_parent()

var _client = WebSocketClient.new()

func _ready():
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	_client.connect("data_received", self, "_on_data")

	var err = _client.connect_to_url(websocket_url)
	if err != OK:
		print("Unable to connect")
		set_process(false)

func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	set_process(false)

func _connected(proto = ""):
	print("Connected with protocol: ", proto)
	_client.get_peer(1).put_packet("Test packet".to_utf8())

func _on_data():
	var raw_data = _client.get_peer(1).get_packet().get_string_from_utf8()
	var data = JSON.parse(raw_data).result
	
	if data != null:
		if data.has("hand"):
			var new_point = Point.new() as Point
			new_point.index = data["index"]
			new_point.x = data["x"]
			new_point.y = data["y"]
			new_point.z = data["z"]
			
			if data["hand"] == "Left":
				parent.handL.points[new_point.index] = new_point
			elif data["hand"] == "Right":
				parent.handR.points[new_point.index] = new_point
		
		if data.has("distanceL"):
			parent.handL.distance = data["distanceL"]
			parent.handR.distance = data["distanceR"]


func _process(_delta):
	_client.poll()
