extends Node2D

onready var screen_width: float = get_viewport_rect().size.x

onready var labelPointsL = $LabelPointsL.get_children()
onready var handPointsL = $HandPointsL.get_children()
onready var labelDistanceL = $LabelDistanceL
onready var handL: Hand = GameManager.handL

onready var labelPointsR = $LabelPointsR.get_children()
onready var handPointsR = $HandPointsR.get_children()
onready var labelDistanceR = $LabelDistanceR
onready var handR: Hand = GameManager.handR

var debugActivated: bool = false

func _ready():
	handL.handType = "Left"
	handR.handType = "Right"
	for _i in range(21):
		handL.points.append(Point.new())
		handR.points.append(Point.new())
	GameManager.client = self
	#open_external_exe()

func _input(_event):
	if Input.is_action_just_pressed("Enter"):
		ActivateDebug()

func _process(_delta):
	MoveHandPoints(handL)
	MoveHandPoints(handR)
	if debugActivated:
		ShowDebug()

func MoveHandPoints(hand: Hand):
	for i in range(21):
		var targetX = hand.points[i-1].x * screen_width
		var targetY = hand.points[i-1].y * screen_width
		if hand.handType == "Left":
			handPointsL[i-1].set_position(Vector2(targetX, targetY))
		elif hand.handType == "Right":
			handPointsR[i-1].set_position(Vector2(targetX, targetY))

func ActivateDebug():
	debugActivated = !debugActivated
	for i in range(21):
		labelPointsL[i].visible = debugActivated
		labelPointsR[i].visible = debugActivated
	labelDistanceL.visible = !labelDistanceL.visible
	labelDistanceR.visible = !labelDistanceR.visible

func ShowDebug():
	for i in range(21):
		labelPointsL[i-1].text = str(handL.points[i-1].index) + ": " + str(handL.points[i-1].x) + ", " + str(handL.points[i-1].y) + ", " + str(handL.points[i-1].z)
		labelPointsR[i-1].text = str(handR.points[i-1].index) + ": " + str(handR.points[i-1].x) + ", " + str(handR.points[i-1].y) + ", " + str(handR.points[i-1].z)
	labelDistanceL.text = str(handL.distance)
	labelDistanceR.text = str(handR.distance)

func open_external_exe():
	var path = "X:/User/Documents/Godot/Client/Aplicacion.exe"
	print(path)
	var error = OS.execute(path, []) # Ejecutar el archivo
	if error != OK:
		print("Error al abrir el archivo ejecutable:", path)
	else:
		print("Archivo ejecutable abierto correctamente:", path)
