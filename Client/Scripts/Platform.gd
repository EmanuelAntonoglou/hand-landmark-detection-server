extends RigidBody

var Ltouch: bool = false
var canDetectTouchL: bool = true

var Rtouch: bool = false
var canDetectTouchR: bool = true

func _process(_delta):
	if GameManager.handL.distance <= 25 and canDetectTouchL:
		Ltouch = !Ltouch
		canDetectTouchL = false
	if GameManager.handL.distance > 25:
		canDetectTouchL = true
	if GameManager.handR.distance <= 25 and canDetectTouchR:
		Rtouch = !Rtouch
		canDetectTouchR = false
	if GameManager.handR.distance > 25:
		canDetectTouchR = true
	

func _physics_process(_delta):
	RotatePlatform()

func RotatePlatform():
	rotation_degrees.x = CalculateRotation(GameManager.handL, Ltouch)
	rotation_degrees.z = CalculateRotation(GameManager.handR, Rtouch)

func CalculateRotation(hand: Hand, touch) -> float:
	var min_hand: int = 25
	var max_hand: int = 100
	var dis_hand: float = clamp(hand.distance, min_hand, max_hand)
	var min_rot: int = 0
	var max_rot: int = 45

	var range_hand: float = max_hand - min_hand
	var range_rot: float = max_rot - min_rot
	var percentage: float = (dis_hand - min_hand) / range_hand
	var final_rot: float = min_rot + percentage * range_rot
	
	if touch:
		return final_rot
	else:
		return final_rot * -1
