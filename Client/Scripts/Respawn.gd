extends Area

onready var level = get_parent()
onready var respawn = level.get_node("Respawn")

var ball_scene: PackedScene = preload("res://Scenes/Ball.tscn")


func Respawn():
	var ball_instance = ball_scene.instance()
	level.add_child(ball_instance)
	ball_instance.translation = respawn.translation

func _on_Area_area_entered(_area):
	Respawn()
