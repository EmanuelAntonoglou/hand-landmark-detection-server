import cv2
import mediapipe as mp
import threading

import asyncio
import websockets
import json

cap = cv2.VideoCapture(0)
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_face_mesh = mp.solutions.face_mesh

face_mesh = mp_face_mesh.FaceMesh(
    min_detection_confidence=0.5, min_tracking_confidence=0.5
)


def OpenAppFace():
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignorando el marco de la cámara vacío.")
            # Si se carga un video, use 'break' aquí para evitar un bucle infinito
            continue

        image = cv2.flip(image, 1) # Voltear la imagen horizontalmente para una imagen de espejo natural

        # Convertir la imagen a formato RGB
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False

        # Detectar rostros
        results = face_mesh.process(image)

        # Dibujar puntos de referencia y calcular EAR si se detectan rostros
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        if results.multi_face_landmarks:
            for face_landmarks in results.multi_face_landmarks:
                mp_drawing.draw_landmarks(
                    image=image,
                    landmark_list=face_landmarks,
                    connections=mp_face_mesh.FACEMESH_TESSELATION,
                    landmark_drawing_spec=None,
                    connection_drawing_spec=mp_drawing_styles.get_default_face_mesh_tesselation_style(),
                )

                # Calcular EAR normalizado e imprimir resultados
                avg_ear = calculate_normalized_ear(face_landmarks.landmark)
                eye_state = "Abierto" if avg_ear >= 0.66 else "Cerrado"
                print(str(round(avg_ear, 2)) + ": " + eye_state)


        # Mostrar la imagen
        cv2.imshow("Face Detection", image)
        if cv2.waitKey(5) & 0xFF == 27:
            break

    cap.release()
    cv2.destroyAllWindows()

def calculate_normalized_ear(landmark_list):
    # Obtener coordenadas de los puntos de referencia clave del ojo
    left_eye = [landmark_list[362], landmark_list[374], landmark_list[386], landmark_list[380]]
    right_eye = [landmark_list[334], landmark_list[293], landmark_list[249], landmark_list[261]]

    # Calcular la distancia horizontal entre los puntos de referencia
    left_eye_h = abs(left_eye[0].x - left_eye[2].x)
    right_eye_h = abs(right_eye[0].x - right_eye[2].x)

    # Calcular la distancia pupilar (asumiendo que los puntos de referencia 374 y 334 representan las pupilas)
    pupillary_distance = abs(landmark_list[374].x - landmark_list[334].x)

    # Calcular EAR normalizado para cada ojo y promediar
    left_ear = left_eye_h / pupillary_distance
    right_ear = right_eye_h / pupillary_distance
    avg_ear = (left_ear + right_ear) / 2

    return avg_ear

#endregion



if __name__ == '__main__':
    thread_open_app = threading.Thread(target=OpenAppFace)

    # Iniciar hilos
    thread_open_app.start()

    # Esperar a que los hilos terminen (opcional)
    thread_open_app.join()
